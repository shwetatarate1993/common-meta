declare const _default: {
    configItem: {
        configObjectId: string;
        name: string;
        configObjectType: string;
        projectId: number;
        createdBy: any;
        itemDescription: any;
        creationDate: any;
        updatedBy: any;
        updationDate: any;
        deletionDate: any;
        isDeleted: number;
    };
    configItemProperty: {
        propertyId: string;
        propertyName: string;
        propertyValue: string;
        itemId: string;
        createdBy: any;
        isDeleted: number;
        creationDate: any;
        updatedBy: any;
        updationDate: any;
        deletionDate: any;
    }[];
    configItemRelation: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: any;
        isDeleted: number;
        creationDate: any;
        updatedBy: any;
        updationDate: any;
        deletionDate: string;
    }[];
    configItemPrivilege: {
        privilegeId: string;
        privilegeType: string;
        itemId: string;
        roleId: number;
        createdBy: any;
        isDeleted: number;
        creationDate: any;
        updatedBy: any;
        updationDate: any;
        deletionDate: string;
    }[];
};
export default _default;
//# sourceMappingURL=card.configmetadata.d.ts.map