declare const _default: {
    configObjectId: string;
    name: string;
    configObjectType: string;
    projectId: number;
    createdBy: any;
    itemDescription: any;
    creationDate: any;
    updatedBy: any;
    updationDate: any;
    deletionDate: any;
    isDeleted: number;
    displayLabel: string;
    renderingBeanName: string;
    searchEnabled: boolean;
    defaultDisplay: boolean;
    cardType: string;
    chartType: any;
    dataGridId: string;
    logicalEntityId: string;
    privileges: {
        privilegeId: string;
        privilegeType: string;
        itemId: string;
        roleId: number;
        createdBy: any;
        isDeleted: number;
        creationDate: any;
        updatedBy: any;
        updationDate: any;
        deletionDate: string;
    }[];
    childRelations: any[];
    parentRelations: {
        relationId: string;
        relationType: string;
        parentItemId: string;
        childItemId: string;
        createdBy: any;
        isDeleted: number;
        creationDate: any;
        updatedBy: any;
        updationDate: any;
        deletionDate: string;
    }[];
};
export default _default;
//# sourceMappingURL=cardobject.d.ts.map