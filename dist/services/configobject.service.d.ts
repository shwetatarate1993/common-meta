import { AppConfigDBClient, ConfigItem, ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation, ConfigMetadata } from 'meta-db';
export declare class ConfigService {
    dbClient: AppConfigDBClient;
    constructor(dbClientName?: string);
    createConfig<T extends ConfigItem>(config: T, parseObjectToConfig: (configObject: T) => ConfigMetadata, parseConfigToObject: (config: ConfigItemModel, prop: ConfigItemProperty[], rel: ConfigItemRelation[], priv: ConfigItemPrivilege[]) => T): Promise<T>;
    createProperty(properties: ConfigItemProperty[]): Promise<void>;
    createRelation(relations: ConfigItemRelation[], itemId: string): Promise<void>;
    createPrivileges(privileges: ConfigItemPrivilege[]): Promise<void>;
    fetchConfigById<T extends ConfigItem>(id: string, parseConfigToObject: (config: ConfigItemModel, prop: ConfigItemProperty[], rel: ConfigItemRelation[], priv: ConfigItemPrivilege[]) => T): Promise<T>;
    fetchConfigByType<T extends ConfigItem>(type: string, parseConfigToObject: (config: ConfigItemModel, prop: ConfigItemProperty[], rel: ConfigItemRelation[], priv: ConfigItemPrivilege[]) => T): Promise<T[]>;
    fetchConfigsByParentId<T extends ConfigItem>(parentId: string, parseConfigToObject: (config: ConfigItemModel, prop: ConfigItemProperty[], rel: ConfigItemRelation[], priv: ConfigItemPrivilege[]) => T): Promise<T[]>;
    fetchConfigsByChildId<T extends ConfigItem>(childId: string): Promise<ConfigItemModel[]>;
    fetchConfigsByParentIdAndRelationType<T extends ConfigItem>(parentId: string, relationType: string, parseConfigToObject: (config: ConfigItemModel, prop: ConfigItemProperty[], rel: ConfigItemRelation[], priv: ConfigItemPrivilege[]) => T): Promise<T[]>;
    fetchChildNodesAsPerDisplayType<T extends ConfigItem>(parentId: string, relationType: string, displayType: string, isCombined: string, parseConfigToObject: (config: ConfigItemModel, prop: ConfigItemProperty[], rel: ConfigItemRelation[], priv: ConfigItemPrivilege[]) => T): Promise<T[]>;
    fetchConfigItemsForIdList<T extends ConfigItem>(idList: string[], parseConfigToObject: (config: ConfigItemModel, prop: ConfigItemProperty[], rel: ConfigItemRelation[], priv: ConfigItemPrivilege[]) => T): Promise<T[]>;
    processConfigObjects<T extends ConfigItem>(configItem: ConfigItemModel[], parseConfigToObject: (config: ConfigItemModel, prop: ConfigItemProperty[], rel: ConfigItemRelation[], priv: ConfigItemPrivilege[]) => T): Promise<T[]>;
    processConfig<T extends ConfigItem>(item: ConfigItemModel, parseConfigToObject: (config: ConfigItemModel, prop: ConfigItemProperty[], rel: ConfigItemRelation[], priv: ConfigItemPrivilege[]) => T): Promise<T>;
}
export declare const getObject: (dbClient: string) => ConfigService;
declare const configService: ConfigService;
export default configService;
//# sourceMappingURL=configobject.service.d.ts.map