"use strict";
/* tslint:disable:align */
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class ConfigService {
    constructor(dbClientName) {
        if (dbClientName === undefined || dbClientName === null) {
            this.dbClient = meta_db_1.getAppConfigDBClient('METADB');
        }
        else {
            this.dbClient = meta_db_1.getAppConfigDBClient(dbClientName);
        }
    }
    async createConfig(config, parseObjectToConfig, parseConfigToObject) {
        const configObject = parseObjectToConfig(config);
        try {
            const result = await this
                .dbClient
                .upsertConfigItem(configObject.configItem);
            await this.createProperty(configObject.configItemProperty);
            await this.createRelation(configObject.configItemRelation, configObject.configItem.configObjectId);
            await this.createPrivileges(configObject.configItemPrivilege);
        }
        catch (error) {
            throw error;
        }
        const cloneObject = parseConfigToObject(configObject.configItem, configObject.configItemProperty, configObject.configItemRelation, configObject.configItemPrivilege);
        return cloneObject;
    }
    async createProperty(properties) {
        try {
            await this
                .dbClient
                .syncConfigProperty(properties, null);
        }
        catch (error) {
            throw error;
        }
    }
    async createRelation(relations, itemId) {
        try {
            await this
                .dbClient
                .syncConfigRelation(relations, itemId);
        }
        catch (error) {
            throw error;
        }
    }
    async createPrivileges(privileges) {
        try {
            await this
                .dbClient
                .syncConfigPrivilege(privileges);
        }
        catch (error) {
            throw error;
        }
    }
    async fetchConfigById(id, parseConfigToObject) {
        const configItem = await this
            .dbClient
            .fetchConfigItemById(id);
        if (configItem) {
            const configObject = await this.processConfig(configItem, parseConfigToObject);
            return configObject;
        }
        else {
            return null;
        }
    }
    async fetchConfigByType(type, parseConfigToObject) {
        const configItems = await this
            .dbClient
            .fetchConfigItemByType(type);
        const configObjects = await this.processConfigObjects(configItems, parseConfigToObject);
        return configObjects;
    }
    async fetchConfigsByParentId(parentId, parseConfigToObject) {
        const configItems = await this
            .dbClient
            .fetchConfigItemByParentId(parentId);
        const configObjects = await this.processConfigObjects(configItems, parseConfigToObject);
        return configObjects;
    }
    async fetchConfigsByChildId(childId) {
        const configItems = await this
            .dbClient
            .fetchConfigItemByChildId(childId);
        if (configItems.length > 0) {
            return configItems;
        }
        else {
            return null;
        }
    }
    async fetchConfigsByParentIdAndRelationType(parentId, relationType, parseConfigToObject) {
        const configItems = await this
            .dbClient
            .fetchConfigItemByParentIdAndRelationType(parentId, relationType);
        const configObjects = await this.processConfigObjects(configItems, parseConfigToObject);
        return configObjects;
    }
    async fetchChildNodesAsPerDisplayType(parentId, relationType, displayType, isCombined, parseConfigToObject) {
        const configItems = await this
            .dbClient
            .fetchChildNodesAsPerDisplayType(parentId, relationType, displayType, isCombined);
        const configObjects = await this.processConfigObjects(configItems, parseConfigToObject);
        return configObjects;
    }
    async fetchConfigItemsForIdList(idList, parseConfigToObject) {
        const configItems = await this
            .dbClient
            .fetchConfigItemsForIdList(idList);
        const configObjects = await this.processConfigObjects(configItems, parseConfigToObject);
        return configObjects;
    }
    async processConfigObjects(configItem, parseConfigToObject) {
        const configObjects = [];
        if (configItem.length > 0) {
            for (const item of configItem) {
                if (item) {
                    const object = await this.processConfig(item, parseConfigToObject);
                    configObjects.push(object);
                }
            }
            return configObjects;
        }
        else {
            return null;
        }
    }
    async processConfig(item, parseConfigToObject) {
        const properties = await this
            .dbClient
            .fetchConfigPropsByConfigId(item.configObjectId);
        const privileges = await this
            .dbClient
            .fetchPrivilegesByItemId(item.configObjectId);
        const relations = await this
            .dbClient
            .fetchAllRelationsByConfigId(item.configObjectId);
        return parseConfigToObject(item, properties, relations, privileges);
    }
}
exports.ConfigService = ConfigService;
// used only in jest
exports.getObject = (dbClient) => {
    return new ConfigService(dbClient);
};
const configService = new ConfigService();
Object.freeze(configService);
exports.default = configService;
//# sourceMappingURL=configobject.service.js.map