"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
const sinon_1 = __importDefault(require("sinon"));
const mockdata_1 = require("../../mockdata");
const configobject_service_1 = require("../configobject.service");
const parseToConfig = sinon_1.default.fake.returns(mockdata_1.configmetadataCard);
const parseToObject = sinon_1.default.fake.returns(mockdata_1.cardObjectMockData);
const knex = meta_db_1.knexClient(meta_db_1.MOCK);
const dbClientCard = configobject_service_1.getObject(meta_db_1.MOCK);
const deleteConfigPropertyItemTable = async () => {
    return await knex('CONFIGITEMPROPERTY').del();
};
const deleteConfigRelationItemTable = async () => {
    return await knex('CONFIGITEMRELATION').del();
};
const deleteConfigItemTable = async () => {
    return await knex('CONFIGITEM').del();
};
const deleteConfigItemPrivileges = async () => {
    return await knex('CONFIGITEMPRIVILEGE').del();
};
const createConfigTable = async () => {
    return knex.schema.createTableIfNotExists('CONFIGITEM', (table) => {
        table.string('ITEMID');
        table.string('ITEMNAME');
        table.string('ITEMTYPE');
        table.string('ITEMDESCRIPTION');
        table.string('PROJECTID');
        table.string('CREATEDBY');
        table.string('UPDATEDBY');
        table.timestamp('CREATIONDATE');
        table.timestamp('UPDATIONDATE');
        table.integer('ISDELETED');
        table.timestamp('DELETIONDATE');
    });
};
const createConfigPropTable = async () => {
    return knex.schema.createTableIfNotExists('CONFIGITEMPROPERTY', (table) => {
        table.string('ITEMID');
        table.string('PROPERTYID');
        table.string('PROPERTYNAME');
        table.string('PROPERTYVALUE');
        table.string('CREATEDBY');
        table.string('UPDATEDBY');
        table.timestamp('CREATIONDATE');
        table.timestamp('UPDATIONDATE');
        table.integer('ISDELETED');
        table.timestamp('DELETIONDATE');
    });
};
const createConfigRelTable = async () => {
    return knex.schema.createTableIfNotExists('CONFIGITEMRELATION', (table) => {
        table.string('RELATIONID');
        table.string('RELATIONTYPE');
        table.string('PARENTITEMID');
        table.string('CHILDITEMID');
        table.string('GROUPITEMID');
        table.string('CREATEDBY');
        table.string('UPDATEDBY');
        table.timestamp('CREATIONDATE');
        table.timestamp('UPDATIONDATE');
        table.integer('ISDELETED');
        table.timestamp('DELETIONDATE');
    });
};
const createPrivilegeTable = async () => {
    return knex.schema.createTableIfNotExists('CONFIGITEMPRIVILEGE', (table) => {
        table.string('PRIVILEGEID');
        table.integer('ROLEID');
        table.string('ITEMID');
        table.string('PRIVILEGETYPE');
        table.string('CREATEDBY');
        table.string('UPDATEDBY');
        table.timestamp('CREATIONDATE');
        table.timestamp('UPDATIONDATE');
        table.integer('ISDELETED');
        table.timestamp('DELETIONDATE');
    });
};
const tableSetup = async () => {
    const tableCreated = await createConfigTable();
    const propTableCreated = await createConfigPropTable();
    const relTableCreated = await createConfigRelTable();
    const privilegeTableCreated = await createPrivilegeTable();
    const tableDeletedProp = await deleteConfigPropertyItemTable();
    const tableDeletedRel = await deleteConfigRelationItemTable();
    const tableDeleted = await deleteConfigItemTable();
    const privilegesDeleted = await deleteConfigItemPrivileges();
};
test('Services for CardObject', async () => {
    await tableSetup();
    try {
        await dbClientCard.createConfig(mockdata_1.cardObjectMockData, parseToConfig, parseToObject);
        const servicesMock = sinon_1.default.mock(dbClientCard.dbClient);
        servicesMock.expects('upsertConfigItem').atLeast(1).returns(1);
        servicesMock.expects('syncConfigProperty').atLeast(1);
        servicesMock.expects('syncConfigRelation').atLeast(1);
        servicesMock.expects('syncConfigPrivilege').atLeast(1);
        servicesMock.expects('fetchConfigItemById').atLeast(1).returns(mockdata_1.configmetadataCard.configItem);
        servicesMock.expects('fetchConfigPropsByConfigId').atLeast(1).returns(mockdata_1.configmetadataCard.configItemProperty);
        servicesMock.expects('fetchPrivilegesByItemId').atLeast(1).returns(mockdata_1.configmetadataCard.configItemPrivilege);
        servicesMock.expects('fetchAllRelationsByConfigId').atLeast(1).returns(mockdata_1.configmetadataCard.configItemRelation);
        const configList = [];
        configList.push(mockdata_1.configmetadataCard.configItem);
        servicesMock.expects('fetchConfigItemByParentId').atLeast(1).returns(configList);
        const card = await dbClientCard.fetchConfigById(mockdata_1.cardObjectMockData.configObjectId, parseToObject);
        const cardByGroupId = await dbClientCard.fetchConfigsByParentId(mockdata_1.cardObjectMockData.parentRelations[0].parentItemId, parseToObject);
        sinon_1.default.assert.match(card.privileges.length, mockdata_1.cardObjectMockData.privileges.length);
        sinon_1.default.assert.match(card.parentRelations.length, mockdata_1.cardObjectMockData.parentRelations.length);
        sinon_1.default.assert.match(cardByGroupId[0].parentRelations.length, mockdata_1.cardObjectMockData.parentRelations.length);
        sinon_1.default.assert.match(cardByGroupId[0].parentRelations[0].relationType, 'CardGroup_Card');
        sinon_1.default.assert.match(card.configObjectType, 'Card');
        servicesMock.restore();
    }
    catch (error) {
        throw error;
    }
});
//# sourceMappingURL=configobject.service.test.js.map