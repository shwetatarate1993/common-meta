"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_1 = require("meta-db");
class CommonMetaConfig {
    static configure() {
        meta_db_1.MetaDBConfig.configure('METADB');
    }
    constructor() {
        /** No Op */
    }
}
exports.default = CommonMetaConfig;
//# sourceMappingURL=common.meta.config.js.map